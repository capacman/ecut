extern crate getopts;
use getopts::{optopt,optflag,getopts,OptGroup,reqopt,usage};
use std::os;
use std::io::{File,BufferedReader};
use std::num;
fn main() {
	let args=os::args();
	let program=args[0].clone();
	let mut delimiter='\t';
	let mut fields = vec![0u];

	let opts =  [
		reqopt("f","fields","select only those fields","NAME"),
		optopt("d","delimiter","delimiter to use","NAME")
	];

	let matches = match getopts(args.tail(),opts) {
		Ok(m)=>{m}
		Err(f)=>{
			println!("{}",f.to_string());
			print_usage(program.as_slice(),opts);
			return;
		}
	};

	let input = if !matches.free.is_empty() {
		matches.free[0].clone()
	} else {
		print_usage(program.as_slice(),opts);
		return;
	};

	if matches.opt_present("d") {
		delimiter=matches.opt_str("d").unwrap().as_slice().char_at(0);
	}

	if matches.opt_present("f") {
		let str_arg=matches.opt_str("f").unwrap();

		let number_tuples:Vec<(&str,Option<uint>)>=str_arg.as_slice().split(delimiter).map(|slice| {(slice,from_str::<uint>(slice))}).collect();

		let non_number:Option<&(&str,Option<uint>)>=number_tuples.iter().find(|option| {
			match **option {
				(_,Some(_)) => {
					false
				},
				(_,None) => true
			}
		});
		
		match non_number {
			Some(&(str,None)) => {fail!("{} is not an uint",str)},
			_ => {}
		}
		let mut numbers=number_tuples.iter().map(|tuple| {

			let (_,number_option)=*tuple;
			number_option.unwrap()-1u
		});
		fields=numbers.collect::<Vec<uint>>();
		fields.sort();
	}

	println!("input is {}",input);
	let del_str=std::string::String::from_char(1,delimiter);
	println!("delimiter is {}",if delimiter=='\t' {"\\t"} else {del_str.as_slice()});
	read_file(&Path::new(input),fields.as_slice());
}

fn print_usage(program:&str,group:&[OptGroup]){
	println!("{}",usage(format!("brief usage for {}",program).as_slice(),group));
}

fn read_file(path:&Path,fields:&[uint]){
	let file=match File::open(path) {
		Err(why) => fail!("could not open {} : {}",path.display(),why.desc),
		Ok(file)=>file
	};
	let mut reader = BufferedReader::new(file);

	for line_result in reader.lines() {
		let line:String=line_result.unwrap();
		println!("{}",line);
		let vec=line.as_slice().split(',').collect::<Vec<&str>>();
		for &i in fields.iter() {
			println!("{}",vec[i]);
		}
	}
}

